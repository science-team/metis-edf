      subroutine lect11(n,long,xadj,adjncy,niv,ifm)
      implicit none
      integer xadj(*), adjncy(*),n,long,i,ll,j,deb,fin,niv
      integer ifm
c      open(UNIT=81,FILE='fort.81',FORM='FORMATTED')
c      
      rewind 81
      read(81,1001) n,ll,niv,ifm
 1001       FORMAT(8I10)
c
      if(niv.GE.1) then
      write(ifm,*) 'Metis nombres de noeuds, d''aretes  ', n,long
      endif
      read(81,1001) (xadj(i),i=1,n+1)
 1000       FORMAT(10I8)

      if(niv.GE.1) then
      write(ifm,*) 'Metis Tableau Xadj lu  '
      endif
      do j=1,n
      deb=xadj(j)
      fin = xadj(j+1)-1
      read(81,1000) (adjncy(i),i=deb,fin)
      enddo
      if(niv.GE.1) then
      write(ifm,*) 'Metis Tableau Adjncy lu  '
      endif
      do i=1,n+1
      xadj(i)= xadj(i)-1
      enddo
      do i=1,long
      adjncy(i)= adjncy(i) - 1
      enddo
      end  
