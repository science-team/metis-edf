# WARNING : This file has been modified for Code_Aster
#           http://www.code-aster.org					  

SHELL=/bin/sh
SUBDIRS=Lib Programs
CONFDIR=./CONFIG

default: Makefile.in onmetis.exe pmetis kmetis tests

onmetis.exe: Makefile.in
	for d in $(SUBDIRS); do		\
		(cd $$d && $(MAKE));	 \
	done

pmetis: Makefile.in
	for d in $(SUBDIRS); do		\
		(cd $$d && $(MAKE));	 \
	done

kmetis: Makefile.in
	for d in $(SUBDIRS); do		\
		(cd $$d && $(MAKE));	 \
	done



tests:
	cd Test ; make

clean:
	for d in $(SUBDIRS) Test; do		\
		(cd $$d && $(MAKE) $@); \
	done

distclean:
	for d in $(SUBDIRS) Test; do		\
		(cd $$d && $(MAKE) $@); \
	done
	-$(RM) Makefile.in

#################################################################
# config : define variables depends on platform
#  Linux, IRIX64, SunOS (SOLARIS/SOLARIS64), OSF1 (TRU64)
Makefile.in:
	sh $(CONFDIR)/configure

